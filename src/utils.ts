import { CACHE_KEY_PREFIX_FQC, CACHE_KEY_PREFIX_NODE_FQC } from './enums'
import { InMemoryLRUCache } from 'apollo-server-caching'

export type Node = { type: string; id: string }

/**
 * Generate cache key of Node and FQC hashes array mapping
 *
 * e.g. ['Article', 18] -> 'node-fqcs:Article:18'
 */
export const toNodeFQCKey = (node: Node): string => {
  return `${CACHE_KEY_PREFIX_NODE_FQC}:${node.type}:${node.id}`
}

/**
 * Record Node:FQC mapping
 *
 */
export const recordNodeFQCMapping = ({
  nodeFQCKeys,
  fqcKey,
  ttl,
  redis,
}: {
  nodeFQCKeys: string[]
  fqcKey: string
  ttl: number
  redis: any
}): void => {
  try {
    nodeFQCKeys.forEach((cacheKey: string) => {
      redis.set(cacheKey, fqcKey, { ttl })
        .catch(console.warn)
    })
  } catch (error) {
    console.warn(error)
  }
}

/**
 * Invalidate full query caches by the given related node keys
 */
export const invalidateFQC = async ({
  node,
  cache,
}: {
  node: Node
  cache: InMemoryLRUCache
}): Promise<void> => {
  try {
    const key = toNodeFQCKey(node)
    const item = await cache.get(key)

    cache.delete('fqc:' + item)
    cache.delete(key)

  } catch (error) {
    console.warn(error)
  }
}
